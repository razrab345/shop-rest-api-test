<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    #[Route('/products', name: 'app_products')]
    public function index(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        if (!$request->request->has('type_id')) {
            return throw new JsonException('Product type not found');
        }

        if (!$productType = $doctrine->getRepository(ProductType::class)->find($request->request->get('type_id'))) {
            return throw new JsonException('Product type not found');
        }

        return $this->json([
            'products' => $productType->getProducts()
        ]);
    }
}
