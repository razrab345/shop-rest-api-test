<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Entity\Product;
use DateTimeImmutable;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart', name: 'app_cart')]
    public function index(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $sessionCart = $request->getSession()->get('cartID');

        if (empty($sessionCart)) {
            if (!$user = $this->getUser()) {
                return throw new JsonException('User not found');
            }

            $cart = $doctrine->getRepository(Cart::class)->findOneBy([
                'user' => $user
            ]);
        } else {
            $cart = $doctrine->getRepository(Cart::class)->find($sessionCart);
        }

        return $this->json([
            'qty' => $cart->getQty(),
            'amount' => $cart->getAmount(),
            'products' => $cart->getCartProducts()
        ]);
    }

    #[Route('/cart/add', name: 'app_cart_add')]
    public function add(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $result = [
            'status' => false
        ];

        if (!$request->request->has('product_id')) {
            return throw new JsonException('Product not found');
        }

        if (!$product = $doctrine->getRepository(Product::class)->find($request->request->get('product_id'))) {
            return throw new JsonException('Product not found');
        }

        if (!$user = $this->getUser()) {
            return throw new JsonException('User not found');
        }

        $sessionCart = $request->getSession()->get('cartID');

        if (empty($sessionCart)) {
            $cart = $doctrine->getRepository(Cart::class)->findOneBy([
                'user' => $user
            ]);
        } else {
            $cart = $doctrine->getRepository(Cart::class)->find($sessionCart);
        }

        $doctrine->getConnection()->beginTransaction();

        try{
            $entityManager = $doctrine->getManager();

            if (!$cart) {
                $cart = new Cart();
                $cart->setUser($user);
                $cart->setQty(1);
                $cart->setAmount($product->price);
                $cart->setCreatedAt((new DateTimeImmutable));
                $entityManager->persist($cart);
            } else {
                $cart->setQty($cart->getQty() + 1);
                $cart->setAmount($cart->getAmount() + $product->price);
            }

            $request->getSession()->set('cartID', $cart->getId());

            $cartProduct = new CartProduct();
            $cartProduct->setCart($cart);
            $cartProduct->setProduct($product);

            $entityManager->persist($cartProduct);
            $entityManager->flush();

            $doctrine->getConnection()->commit();
            $result['message'] = 'Product adding cart!';
            $result['status'] = true;
        } catch (Exception $e) {
            $doctrine->getConnection()->rollBack();
            $result['message'] = $e->getMessage();
        }

        return $this->json($result);
    }

    #[Route('/cart/remove', name: 'app_cart_remove')]
    public function remove(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $result = [
            'status' => false,
            'message' => 'Cart not found'
        ];

        if (!$request->request->has('product_id')) {
            return throw new JsonException('Product not found');
        }

        if (!$product = $doctrine->getRepository(Product::class)->find($request->request->get('product_id'))) {
            return throw new JsonException('Product not found');
        }

        if (!$user = $this->getUser()) {
            return throw new JsonException('User not found');
        }

        $sessionCart = $request->getSession()->get('cartID');

        if (empty($sessionCart)) {
            $cart = $doctrine->getRepository(Cart::class)->findOneBy([
                'user' => $user
            ]);
        } else {
            $cart = $doctrine->getRepository(Cart::class)->find($sessionCart);
        }

        if (!$cart) {
            return $this->json($result);
        }

        $doctrine->getConnection()->beginTransaction();

        try{
            $entityManager = $doctrine->getManager();

            if ($cart->getQty() === 1) {
                $entityManager->remove($cart);
            } else {
                $cart->setQty($cart->getQty() - 1);
                $cart->setAmount($cart->getAmount() - $product->price);
            }

            $cartProducts = $doctrine->getRepository(CartProduct::class)->findBy([
                'product' => $product
            ]);

            foreach ($cartProducts as $cartProduct) {
                $entityManager->remove($cartProduct);
            }

            $entityManager->flush();

            $doctrine->getConnection()->commit();
            $result['message'] = 'Product remove in cart!';
            $result['status'] = true;
        } catch (Exception $e) {
            $doctrine->getConnection()->rollBack();
            $result['message'] = $e->getMessage();
        }

        return $this->json($result);
    }
}
